--[[
    Basic response setup config time boys
    - Triggers: The key words that will trigger this response, make sure it's specific as if that term matches in the query then that response will be picked.
    - Responses: A list of responses that the AI will randomly pick from when responding to a query (As long as it's not overwritten by the customAction return).
    - customCheck: A custom check of whether a player can *actually* run that query. Return true if you want a player to be able to run it, false if not.
    - customAction: You can have the bot run a customAction if you want. Return false to have the bot pick a random response from the Responses setting,
    return a string to have that overwrite the response with one of your own.
]]--

ASBaseAI.Responses = {
    --[[UPDATE AI DATABASE]]--
    {
        triggers = {
            "System Call, Update AI Database",
            "System Call Update AI Database"
        },

        responses = {
            "Attempting to update AI database..."
        },

        customCheck = function(ply,txt)
            return ply:IsSuperAdmin()
        end,

        customAction = function(ply,txt)
            ASBaseAI.Utils.UpdateDB()
            return false
        end,
    },
    --[[HELLO]]--
    {
        triggers = {
            "Hey",
            "Hi",
            "Hello",
            "Good Morning",
            "Good Afternoon",
            "Good Evening",
        },

        responses = {
            "Hey there %n",
            "Welcome back %n",
            "Hi %n!",
            "Good day to you!"
        },

        customCheck = function(ply,txt)
            return true
        end,

        customAction = function(ply,txt)
            return false
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },
    --[[NAME]]--
    {
        triggers = {
            "What's your name?",
            "Whats your name?",
            "What's your name",
            "Whats your name"
        },

        responses = {
            "I am the Artificial Labile Intelligent Cybernated Existence, Alice for short. Nice to meet you!",
        },

        customCheck = function(ply,txt)
            return true
        end,

        customAction = function(ply,txt)
            return false
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },

    --[[

        MAIN BASE FUNCTIONS

    ]]
    
    {
        triggers = {
            "System Call, Toggle Main Base Gate",
            "System Call, Toggle Main Gate",
        },

        responses = {
            "Confirmed %n, now toggling Main Gate",
            "Understood, Main Gate now toggling...",
        },

        customCheck = function(ply,txt)
            return ply:HasAccessNumber(3)
        end,

        customAction = function(ply,txt)
            if game.GetMap() ~= "rp_titan_rishi_republic2" then return "&4Connection Failed&f - Could not find controls within range." end
            local gate = ents.GetMapCreatedEntity(2593)
            local button = ents.GetMapCreatedEntity(10410)
            if IsDoorOpen(gate) then
                gate:Fire("Close")
                button.ButtonToggled = false
                return "&7Toggled &f - Main Gate closing..."
            else
                gate:Fire("Open")
                button.ButtonToggled = true
                return "&7Toggled &f - Main Gate opening..."
            end
            return false
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },

    {
        triggers = {
            "System Call, Toggle Main Hangar Bay Door",
            "System Call, Toggle Main Hangar Door",
            "System Call, Toggle Main Door",
        },

        responses = {
            "Confirmed %n, now toggling Main Hangar Bay Door",
            "Understood, Main Hangar Bay Door now toggling...",
        },

        customCheck = function(ply,txt)
            return ply:HasAccessNumber(3)
        end,

        customAction = function(ply,txt)
            if game.GetMap() ~= "rp_titan_rishi_republic2" then return "&4Connection Failed&f - Could not find controls within range." end
            local door1 = ents.GetMapCreatedEntity(1695)
            local door2 = ents.GetMapCreatedEntity(1694)
            if IsDoorOpen(door1) then
                door1:Fire("Close")
                door2:Fire("Close")
                return "&7Toggled &f - Main Hangar Bay door closing..."
            else
                door1:Fire("Open")
                door2:Fire("Open")
                return "&7Toggled &f - Main Hangar Bay door opening..."
            end
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },

    {
        triggers = {
            "System Call, Toggle Auxilary Hangar Bay Door",
            "System Call, Toggle Auxilary Hangar Door",
            "System Call, Toggle Auxilary Door",
            "System Call, Toggle Aux Door",
        },

        responses = {
            "Confirmed %n, now toggling Auxilary Hangar Bay Door",
            "Understood, Auxilary Hangar Bay Door now toggling...",
        },

        customCheck = function(ply,txt)
            return ply:HasAccessNumber(3) or ply:HasQualification("pilot")
        end,

        customAction = function(ply,txt)
            if game.GetMap() ~= "rp_titan_rishi_republic2" then return "&4Connection Failed&f - Could not find controls within range." end
            local door1 = ents.GetMapCreatedEntity(2553)
            local door2 = ents.GetMapCreatedEntity(2552)
            if IsDoorOpen(door1) then
                door1:Fire("Close")
                door2:Fire("Close")
                return "&7Toggled &f - Auxilary Hangar Bay closing..."
            else
                door1:Fire("Open")
                door2:Fire("Open")
                return "&7Toggled &f - Auxilary Hangar Bay opening..."
            end
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },

    --[[

        SURVEILLANCE FUNCTIONS

    ]]

    {
        triggers = {
            "System Call, List Active Vehicles",
            "System Call, List Friendly Vehicles",
        },

        responses = nil,

        customCheck = function(ply,txt)
            return ply:HasAccessNumber(4)
        end,

        customAction = function(ply,txt)
            local str = "Listing all friendly in-use vehicles..."
            for _,v in pairs(simfphys.LFS:PlanesGetAll()) do
                if v:GetAI() then continue end
                if not v:GetDriver() or not v:GetDriver():IsPlayer() then continue end
                if not v:GetDriver():lfsGetAITeam() == 2 then continue end
                local name = v.PrintName
                if v.FromVehicleDealer then
                    name = v:GetVehicleName()
                elseif asvehicledealer.vehicles[v:GetClass()] then
                    name = asvehicledealer.vehicles[v:GetClass()].name
                end
                str = str .. "\n&6" .. name .. " &f- &9" .. v:GetDriver():Nick()
            end
            return str
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },

    {
        triggers = {
            "~System Call, Locate Unit"
        },

        responses = nil,

        customCheck = function(ply,txt)
            return ply:HasAccessNumber(4)
        end,

        customAction = function(ply,txt)
            if ply:getJobTable().category == "Staff Jobs" or ply:getJobTable().category == "Event Characters" then return "&4Illegal Target Selected" end
            local newTxt = string.Split(txt, " ")
            if #newTxt > 4 then
                newTxt = table.concat(newTxt, " ", 5, #newTxt)
            else
                newTxt = ""
            end
            if newTxt == "" then return "&4Location Failed &f - You need to specify a target!" end
            for _,v in pairs(player.GetAll()) do
                if string.match(string.lower(v:Nick()),string.lower(newTxt)) then
                    local zone = v:GetZone()
                    if zone and string.match(zone, "_base_") and not v:IsFlagSet(FL_NOTARGET) then
                        return "&aLocation Successful &f- "..v:Nick().." located in &5"..Zones[zone].PrintName
                    else
                        return "&4Location Failed &f - "..v:Nick().." could not be found in internal surveillance zones"
                    end
                end
            end
            return "&4Location Failed &f - No target with that name found"
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },

    {
        triggers = {
            "~System Call, Locate Group"
        },

        responses = nil,

        customCheck = function(ply,txt)
            return ply:HasAccessNumber(4)
        end,

        customAction = function(ply,txt)
            if ply:getJobTable().category == "Staff Jobs" or ply:getJobTable().category == "Event Characters" then return "&4Illegal Target Selected" end
            local newTxt = string.Split(txt, " ")
            if #newTxt > 4 then
                newTxt = table.concat(newTxt, " ", 5, #newTxt)
            else
                newTxt = ""
            end
            if newTxt == "" then return "&4Location Failed &f - You need to specify an identifier!" end
            local aliases = {
                ["41st Elite Corps"] = "41st",
                ["212th Attack Battalion"] = "212th",
                ["501st Legion"] = "501st",
                ["Clone Guard"] = "cg",
            }
            newTxt = aliases[newTxt] or newTxt
            local finalMes = ""
            for _,v in pairs(player.GetAll()) do
                if string.match(string.lower(v:getJobTable().category),string.lower(newTxt)) then
                    local zone = v:GetZone()
                    if zone and string.match(zone, "_base_") and not v:IsFlagSet(FL_NOTARGET) then
                        finalMes = finalMes.."\n&f- "..v:Nick().." located in &5"..Zones[zone].PrintName
                    else
                        finalMes = finalMes.."\n&f- "..v:Nick().." &4could not be located"
                    end
                end
            end
            if finalMes ~= "" then
                return "&aLocation of all units with identifier:\n"..finalMes
            else
                return "&4Location Failed &f - No targets with that identifier found"
            end
        end,

        canSee = function(ply,text,activator)
            return ply == activator
        end,
    },
}
